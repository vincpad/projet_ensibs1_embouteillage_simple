// Projet d'algorithmique d'octobre-décembre 2014 - "Embouteillages"
// Vincent Rocher
// ENSIBS1 - TP4

// Suivi des modifications : https://bitbucket.org/vincpad/projet_ensibs1_embouteillage_simple


//---------------- Inclusions nécessaires au fonctionnement du programme -----------------//
#include <iostream>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

using namespace std;


//------------------------ Déclaration des fonctions du programme ------------------------//
void generererMatrice();    // Remplit la matrice d'affichage ainsi que les matrices d'empreinte en fonction des véhicules existants
void afficher();    // Affiche le contenu de la matrice d'affichage à l'écran (avec les ids des lignes et colonnes)
void bouger(int idVehicule, int decalage); // Déplace le vehicule <idVehicule> de <decalage> cases (vers la droite/le bas si positif, l'inverse si négatif)
void creerVehicules();   // Création manuelle d'un véhicule (par l'utilisateur)
bool presenceCollision(int idVehicule);     // Retourne vrai si une collision est présente (prise en compte du véhicule <idVehicule> uniquement)
bool presenceImpossibilite(int idVehicule); // Retourne vrai si un véhicule est à une position pouvant créer une configuration irrésolvable (prise en compte du véhicule <idVehicule> uniquement)
char attendreActionClavier();   // Fonction retournant un caractère lors de l'appui de certaines touches, le caractère étant fonction de la touche enfoncée
bool nouveauVehicule(int id, bool bus, bool direction, int posX, int posY, bool valider);   // Création d'un véhicule avec ses paramètres (le paramètres <valider> permet de créer de vehicule juste pour l'affichage (lors du placement) (lorsque valider = false))
bool bougerVehiculesClavier();  // Fonction interprétant les frappes du clavier en déplaçant les véhicules
int nombreVehicules();  // Fonction retournant le nombre de véhicules existants
void rafraichir(string phrase); // Rafraichit l'affichage en effaçant la fenètre puis en insérant un texte <phrase> ainsi que le terrain de jeu
bool jeu_partieDeplacementVehicules(); // Affiche le jeu (déplacement des véhicules ...) et renvoie vrai lorsque le joueur a gagné
void creerVehiculesManuel();    // Affiche la création manuelle des véhicules
void lancerJeu();   // Fonction activée lors de la sélection de l'item "Lancer le jeu" dans le menu principal
void init();    // Commandes effectuées au lancement du jeu
void genererVehicules();    // Génération automatique des véhicules
bool randBool();    // Fonction retournant aléatoirement vrai ou faux
void resetVehicules();  // Réinitialise tous les véhicules (position, état...)
void displayHeader(); // Affiche une entète avec les informations du projet
bool verifierSiGagne(); // Vérifie la position du véhicule de l'utilisateur et renvoie vrai si la partie est gagnée

// Fonctions servant au fonctionnement du menu
string selectionMenu(int item, int itemSelect, int debut); // Fonction affichant les chevrons pour la selection des items dans les menus
void choixMenu(int idMenu, int menuChoisi); // Fonction appelée lors du chois de l'item <menuChoisi> dans le menu <idMenu>
string getModePlacement();  // Retourne le mode de placement choisi ("Automatique" ou "Manuel") pour l'affichage
string getDifficulte(); // Retourne la difficulté choisie pour l'affichage
void menuPrincipal();   // Affiche le menu principal
void menuFinDePartie(); // Affiche le menu de fin de partie (lorsque le joueur a gagné)
void menuAbandon(); // Affiche le menu d'abandon (lorsque le joueur à abandonné)
void menuReglages();    // Affiche le menu de réglahes

// Fonctions utilisées à des fins de test et de debug
void testClavier(); // Fonction retournant l'id de la touche enfoncée sur le clavier, utilisée à des fins de test


//-------------------------- Déclaration des variables globales --------------------------//
int vehiculeSelect = 0; // Véhicule sélectionné 
int itemSelect = 0; // Item sélectionné (dans les menus)
bool modeDePlacement = false; // Vrai si placement automatique
int niveauDeDiff = 0; // Niveau de difficulté pour le placement auto
int nombreMouvements; // Compteur pour le nombre de mouvements effectué

struct vehicule { // structure pour gérer les différents paramètres de chaque véhicule
    int posX, posY; // position en x et y du coin  supérieur gauche du véhicule
    bool direction; // vrai si le véhicule est à la verticale
    bool bus; // vrai si le véhicule est un bus
    bool exists; // vrai si le véhicule existe (i.e. a été créé)
};

vehicule vehicules[10]; // Tableau contenant 10 véhicules

char matriceAffichage[6][6]; // Tableau contenant les caractères à afficher
bool empreinteVehicule[10][6][6]; // Matrices d'empreintes pour la gestion des collisions


//-------------------------------------- Programme ---------------------------------------//
int main(){
    init();
    menuPrincipal();
    return 0;
}
void init(){
    srand(time(NULL)); // Nécéssaire au bon fonctionnement de la fonction rand()
    resetVehicules(); // Initialisation des véhicules à l'état "non créé"
}
void genererVehicules(){
    vehicules[0].direction = false;
    vehicules[0].posY = 2;
    vehicules[0].posX = 0;
    vehicules[0].bus = false;
    vehicules[vehiculeSelect].exists = true;
    vehiculeSelect++;
    int nbreV; // Nombre de véhicules à générer
    if(niveauDeDiff == 0) nbreV = int(rand()%2)+4; // entre 4 et 5
    if(niveauDeDiff == 1) nbreV = int(rand()%2)+6; // entre 5 et 6
    if(niveauDeDiff == 2) nbreV = int(rand()%2)+8; // entre 8 et 9
    while(vehiculeSelect <= nbreV){
        do{
            vehicules[vehiculeSelect].exists = true;
            vehicules[vehiculeSelect].posX = int(rand()%6);
            vehicules[vehiculeSelect].posY = int(rand()%6);
            vehicules[vehiculeSelect].bus = randBool();
            vehicules[vehiculeSelect].direction = randBool();
            generererMatrice();
        } while(presenceCollision(vehiculeSelect) || presenceImpossibilite(vehiculeSelect));    // Retenter la création du véhicule si sa création est impossible
        vehiculeSelect++;
    }
}
bool randBool(){
    int r = rand()%2;
    if(r<1) return false;
    else return true;
}
void displayHeader(){
    cout << "PROJET ALGORITHMIQUE 2014\nVINCENT ROCHER - ENSIBS1-TP4\n\n\n";
}
void creerVehiculeUser(){
    vehiculeSelect = 0;
    bool vehiculeCree;
    char actionClavier;
    bool creationTerminee = false;
    vehicules[0].direction = false;
    vehicules[0].posY = 2;
    vehicules[0].bus = false;
    vehiculeCree = false;
    vehicules[vehiculeSelect].exists = true;
    string alerte = "";
    string message = "Deplacez le vehicule a debloquer (vehicule num. 0) ";
    message += " a l'aide des touches flechees. \nValidez avec la touche entree pour passer aux vehicules encombrants.\n";
    rafraichir(message);
    while(!vehiculeCree && !creationTerminee){
        actionClavier = attendreActionClavier();
        alerte = "";
        switch(actionClavier){
            case 'd' :
                vehicules[vehiculeSelect].posX++;
                break;
            case 'g' :
                vehicules[vehiculeSelect].posX--;
                break;
            case 'e' :
                if(presenceCollision(vehiculeSelect) || presenceImpossibilite(vehiculeSelect)) alerte = "Creation impossible";
                else vehiculeCree = true;
                break;
            case 'f' :
                vehicules[vehiculeSelect].exists = false;
                creationTerminee = true;
                alerte = "Vehicule ";
                alerte += vehiculeSelect;
                alerte += " cree !";
                break;
        }
        if(vehicules[vehiculeSelect].posX > 4) vehicules[vehiculeSelect].posX--;
        if(vehicules[vehiculeSelect].posX < 0) vehicules[vehiculeSelect].posX++;
        rafraichir(message);
        if(alerte != "") cout << alerte;
    }
    vehiculeSelect++;
}
bool verifierSiGagne(){
    if(vehicules[0].posX > 4){
        return true;
    }
    else return false;
}
void rafraichir(string phrase){
    generererMatrice();
    system("cls");
    displayHeader();
    cout << phrase << endl;
    afficher();
}
void creerVehicules(){
    bool vehiculeCree;
    char actionClavier;
    bool creationTerminee = false;
    while(!creationTerminee){
        vehiculeCree = false;
        vehicules[vehiculeSelect].exists = true;
        string alerte = "";
        string message = "Deplacez le vehicule num. ";
        message += vehiculeSelect+int('0');
        message += " a l'aide des touches flechees, tournez-le avec t et choisissez son type avec m (voiture ou bus).\nValidez avec la touche entree pour passer au vehicule suivant et terminez avec la touche f\n";
        rafraichir(message);
        while(!vehiculeCree && !creationTerminee){
            actionClavier = attendreActionClavier();
            alerte = "";
            switch(actionClavier){
                case 'h' :
                    vehicules[vehiculeSelect].posY--;
                    break;
                case 'b' :
                    vehicules[vehiculeSelect].posY++;
                    break;
                case 'd' :
                    vehicules[vehiculeSelect].posX++;
                    break;
                case 'g' :
                    vehicules[vehiculeSelect].posX--;
                    break;
                case 't' :
                    vehicules[vehiculeSelect].direction = !vehicules[vehiculeSelect].direction;
                    break;
                case 'm' :
                    vehicules[vehiculeSelect].bus = !vehicules[vehiculeSelect].bus;
                    break;
                case 'e' :
                    if(presenceCollision(vehiculeSelect) || presenceImpossibilite(vehiculeSelect)) alerte = "Creation impossible";
                    else vehiculeCree = true;
                    break;
                case 'f' :
                    vehicules[vehiculeSelect].exists = false;
                    creationTerminee = true;
                    alerte = "Vehicule ";
                    alerte += vehiculeSelect;
                    alerte += " cree !";
                    break;
            }
            if(vehicules[vehiculeSelect].posX > 5) vehicules[vehiculeSelect].posX--;
            if(vehicules[vehiculeSelect].posX < 0) vehicules[vehiculeSelect].posX++;
            if(vehicules[vehiculeSelect].posY > 5) vehicules[vehiculeSelect].posY--;
            if(vehicules[vehiculeSelect].posY < 0) vehicules[vehiculeSelect].posY++;

            rafraichir(message);
            if(alerte != "") cout << alerte;

        }
        vehiculeSelect++;
    }
    vehiculeSelect = 0;
}
bool bougerVehiculesClavier(){
    bool result = false;
    switch(attendreActionClavier()){
        case 'h' :
            if(vehicules[vehiculeSelect].direction) bouger(vehiculeSelect, -1);
            break;
        case 'b' :
            if(vehicules[vehiculeSelect].direction) bouger(vehiculeSelect, 1);
            break;
        case 'd' :
            if(!vehicules[vehiculeSelect].direction) bouger(vehiculeSelect, 1);
            break;
        case 'g' :
            if(!vehicules[vehiculeSelect].direction) bouger(vehiculeSelect, -1);
            break;
        case 'a' :
            vehiculeSelect--;
            if(vehiculeSelect<0) vehiculeSelect=nombreVehicules()-1;
            break;
        case 'z' :
            vehiculeSelect++;
            if(vehiculeSelect>=nombreVehicules()) vehiculeSelect=0;
            break;
        case 'q' :
            result = true;
            break;
    }
    return result;
}
void lancerJeu(){
    resetVehicules();
    if(modeDePlacement == false) creerVehiculesManuel();
    else genererVehicules();

    nombreMouvements = 0;
    bool gagne = jeu_partieDeplacementVehicules();
    if(gagne) menuFinDePartie();
    else menuAbandon();
}
bool jeu_partieDeplacementVehicules(){
    //suivreChemin();
    vehiculeSelect =0;
    bool gagne = false;
    while(true){
        string message = "Selectionnez le vehicule avec les touches a et z, puis deplacez-le avec les touches flechees. \nAbandonnez avec la touche echap.\nVous controlez actuellement le vehicule num. ";
        message += vehiculeSelect+int('0');
        message += ". \n";
        rafraichir(message);
        if(bougerVehiculesClavier()) break;
        if(verifierSiGagne()){
            gagne = true;
            break;
        }
    }
    return gagne;
}
void menuAbandon(){
    itemSelect = 0;
    while(true){
        system("cls");
        cout << "PROJET ALGORITHMIQUE 2014\nVINCENT ROCHER - ENSIBS1-TP4\n\n\n           FIN DE PARTIE\n\n";
        cout << "      Dommage, vous avez abandonne :(\n\n";
        cout << selectionMenu(0, itemSelect, true) << "Reessayer" << selectionMenu(0, itemSelect, false) << endl;
        cout << selectionMenu(1, itemSelect, true) << "Retour au menu principal" << selectionMenu(1, itemSelect, false) << endl;
        char userCmd = attendreActionClavier();
        if(userCmd == 'h') itemSelect--;
        else if(userCmd == 'b') itemSelect++;
        else if(userCmd == 'e'){
            choixMenu(2, itemSelect);
            break;
        } ;
        if(itemSelect<0) itemSelect = 1;
        else if(itemSelect>1) itemSelect = 0;
    }
}
void creerVehiculesManuel(){
    creerVehiculeUser();
    creerVehicules();
}
char attendreActionClavier(){
    bool waiting = true;
    char result;
    while(waiting){
        while(kbhit()){
            switch (getch()){
                case 224:
                    switch(getch()){
                        case 72:
                            result = 'h';
                            waiting = false;
                            break;
                        case 80:
                            result = 'b';
                            waiting = false;
                            break;
                        case 77:
                            result = 'd';
                            waiting = false;
                            break;
                        case 75:
                            result = 'g';
                            waiting = false;
                        break;
                    }
                    break;
                case 97:
                    result = 'a';
                    waiting = false;
                    break;
                case 122:
                    result = 'z';
                    waiting = false;
                    break;
                case 116:
                    result = 't';
                    waiting = false;
                    break;
                case 13:
                    result = 'e';
                    waiting = false;
                    break;
                case 102:
                    result = 'f';
                    waiting = false;
                    break;
                case 109:
                    result = 'm';
                    waiting = false;
                    break;
                case 27:
                    result = 'q';
                    waiting = false;
                    break;    
            }
        }
    }
    return result;
}
bool presenceCollision(int idVehicule){
    bool result = false;
    int c; // Compteur qui va vérifier si il existe plus d'une voiture sur une seule case
    int k;
    for(int i=0; i<6; i++){
        for(int j=0; j<6; j++){
            c=0, k=0;
            while(vehicules[k].exists){
                if(empreinteVehicule[k][i][j]){
                    c++;
                    // cout << "voiture " << k << ", x =" << i << ", y =" << j << ", c =" << c<< endl; // debug collisions
                }
                k++;
            }
            if(c>1){
                result = true;
            }
        }
    }
    if(vehicules[idVehicule].posX<0 || vehicules[idVehicule].posY<0){
        result = true;
    }
    if(vehicules[idVehicule].bus){ // si le véhicule est un bus
        if((!vehicules[idVehicule].direction && vehicules[idVehicule].posX>3)
           ||(vehicules[idVehicule].direction && vehicules[idVehicule].posY>3)){
               result = true;
        }
    }
    else{
        if(((!vehicules[idVehicule].direction && vehicules[idVehicule].posX>4)
           ||(vehicules[idVehicule].direction && vehicules[idVehicule].posY>4)) && idVehicule != 0){ // (ne pas prendre en compte le dépassement du véhicule de l'utilisateur sur la droite)
              result = true;
        }
    }
    return result;
}
bool presenceImpossibilite(int idVehicule){
    bool result = false;
    if(vehicules[idVehicule].direction == false){ // si le véhicule est à l'horizontale
        if(idVehicule != 0 && vehicules[idVehicule].posY == 2) result = true; // Vehicule horizontal sur la ligne C
        if(vehicules[idVehicule].bus == true){
            int i=0;
            while(vehicules[i].exists){
                if(i != idVehicule && vehicules[i].bus == true && vehicules[i].posY == vehicules[idVehicule].posY) result = true; // bus situé sur la même ligne que le bus en cours
                i++;
            }
        }
    }
    if(vehicules[idVehicule].direction == true){ // si le véhicule est à la verticale
        int nbre = 0, i = 0;
        while(vehicules[i].exists){ // Comptage du nombre de voitures verticales sur la même colonne
            if(vehicules[idVehicule].posX == vehicules[i].posX) nbre++;
            i++;
        }
        if(nbre > 2) result = true;

        i = 0;
        while(vehicules[i].exists){
            if(vehicules[i].posX == vehicules[idVehicule].posX){ // même colonne, à la verticale
                if(vehicules[idVehicule].bus == true && vehicules[i].posY > vehicules[idVehicule].posY) result = true; // si le véhicule est un bus vérification qu'il n'y a pas un véhicule à la verticale sous lui
                if(vehicules[i].posY < vehicules[idVehicule].posY && vehicules[i].bus == true) result = true; // vérification qu'il n'y ai pas un bus au dessus
            }
            i++;
        }
    }
    return result;
}
bool nouveauVehicule(int id, bool bus, bool direction, int posX, int posY, bool valider){
    bool result;
    vehicules[id].exists = true;
    vehicules[id].bus = bus;
    vehicules[id].direction = direction;
    vehicules[id].posX = posX;
    vehicules[id].posY = posY;
    generererMatrice();
    afficher();

    if(!valider) vehicules[id].exists = false;
    else if(presenceCollision(id) || presenceImpossibilite(id)){
        cout << "Impossible de placer ce vehicule ici" << endl;
        vehicules[id].exists = false;
        result = false;
        generererMatrice();
    }
    else result = true;
    afficher();
    return result;
}
void bouger(int idVehicule, int decalage){
    if(vehicules[idVehicule].direction){ // si le véhicule est à la verticale
        vehicules[idVehicule].posY = vehicules[idVehicule].posY + decalage;
    }
    else {
        vehicules[idVehicule].posX = vehicules[idVehicule].posX + decalage;
    }
    nombreMouvements++; // Incrémentation du nombre de mouvements
    generererMatrice(); // "Imprimer" le mouvement dans les matrices d'affichage de d'empreinte
    if(presenceCollision(idVehicule) || presenceImpossibilite(idVehicule)){  // Vérifier si le mouvement a donné lieu à une collision
        bouger(idVehicule, -decalage);  // Annuler le mouvement
        nombreMouvements = nombreMouvements - 2;
    }
}
void generererMatrice(){
    // remplir la matrice d'affichage avec des espaces
    for(int k=0; k<6; k++){
        for(int l=0; l<6; l++){
            matriceAffichage[k][l] = ' ';
            for(int m=0; m<10; m++){
                empreinteVehicule[m][k][l] = false;
            }
        }
    }
    int i=0;
    while(vehicules[i].exists){ // tant qu'il existe des véhicules
        // cout << "Traitement du véhicule " << i << endl;
        matriceAffichage[vehicules[i].posX][vehicules[i].posY] = char(int('0')+i); // écrire l'identifiant du véhicule dans la case correpondant au point de reère de celui-ci
        empreinteVehicule[i][vehicules[i].posX][vehicules[i].posY] = true;

        if(vehicules[i].direction){ // si le véhicule est à la verticale
            matriceAffichage[vehicules[i].posX][vehicules[i].posY+1] = char(int('0')+i); // écrire l'identifiant du véhicule dans la case du dessous
            empreinteVehicule[i][vehicules[i].posX][vehicules[i].posY+1] = true; // Ecriture dans la matrice d'empreinte
            if(vehicules[i].bus){ // si le véhicule est un bus
                matriceAffichage[vehicules[i].posX][vehicules[i].posY+2] = char(int('0')+i); // écrire l'identifiant du bus dans la case du dessous
                empreinteVehicule[i][vehicules[i].posX][vehicules[i].posY+2] = true; // Ecriture dans la matrice d'empreinte
            }
        }
        else { // si le véhicule est à l'horizontale
            matriceAffichage[vehicules[i].posX+1][vehicules[i].posY] = char(int('0')+i); // écrire l'identifiant du véhicule dans la case à droite
            empreinteVehicule[i][vehicules[i].posX+1][vehicules[i].posY] = true; // Ecriture dans la matrice d'empreinte
            if(vehicules[i].bus){ // si le véhicule est un bus
                matriceAffichage[vehicules[i].posX+2][vehicules[i].posY] = char(int('0')+i); // écrire l'identifiant du bus dans la case à droite
                empreinteVehicule[i][vehicules[i].posX+2][vehicules[i].posY] = true; // Ecriture dans la matrice d'empreinte
            }
        }
        i++;
    }
}
void afficher(){
    string lettres = "ABCDEF";
    cout << "  1 2 3 4 5 6" << endl;
    for(int i=0; i<6; i++){
        cout << lettres[i] << " ";
        for(int j=0; j<6; j++){
            cout << matriceAffichage[j][i] << " ";
        }
        cout << endl;
    }
}
int nombreVehicules(){
    int n=0;
    while(vehicules[n].exists) n++;
    return n;
}
void resetVehicules(){
    for (int i = 0; i < 10; i++){
        vehicules[i].direction = false;
        vehicules[i].posY = 0;
        vehicules[i].posX = 0;
        vehicules[i].bus = false;
        vehicules[i].exists = false;
    }
}


// Menus
void menuPrincipal(){
    while(true){
        system("cls");
        displayHeader();
        cout << "    MENU PRINCIPAL\n\n";
        cout << selectionMenu(0, itemSelect, true) << "LANCER LE JEU" << selectionMenu(0, itemSelect, false) << endl;
        cout << selectionMenu(1, itemSelect, true) << "REGLAGES" << selectionMenu(1, itemSelect, false) << endl;
        cout << selectionMenu(2, itemSelect, true) << "QUITTER" << selectionMenu(2, itemSelect, false) << endl;
        char userCmd = attendreActionClavier();
        if(userCmd == 'h') itemSelect--;
        else if(userCmd == 'b') itemSelect++;
        else if(userCmd == 'e'){
            choixMenu(0, itemSelect);
            break;
        } ;
        if(itemSelect<0) itemSelect = 2;
        else if(itemSelect>2) itemSelect = 0;
    }
}
void menuFinDePartie(){
    itemSelect = 0;
    while(true){
        system("cls");
        displayHeader();
        cout << "           FIN DE PARTIE\n\n";
        cout << "      Felicitations ! Vous avez gagne !\n\n      Nombre de mouvements effectues : " << nombreMouvements << "\n\n";
        cout << selectionMenu(0, itemSelect, true) << "Rejouer" << selectionMenu(0, itemSelect, false) << endl;
        cout << selectionMenu(1, itemSelect, true) << "Retour au menu principal" << selectionMenu(1, itemSelect, false) << endl;
        char userCmd = attendreActionClavier();
        if(userCmd == 'h') itemSelect--;
        else if(userCmd == 'b') itemSelect++;
        else if(userCmd == 'e'){
            choixMenu(2, itemSelect);
            break;
        } ;
        if(itemSelect<0) itemSelect = 1;
        else if(itemSelect>1) itemSelect = 0;
    }
}
void choixMenu(int idMenu, int menuChoisi){
    if(idMenu == 0){ // Menu principal
        if(menuChoisi==0){
            lancerJeu();
        }
        if(menuChoisi==1){
            itemSelect = 0;
            menuReglages();
        }
        if(menuChoisi==2){
            exit(0);
        }
    }
    else if(idMenu == 1){ // Menu réglages
        if(menuChoisi == 0){
            modeDePlacement = !modeDePlacement;
            menuReglages();
        }
        if(menuChoisi == 1){
            niveauDeDiff++;
            if(niveauDeDiff>2) niveauDeDiff = 0;
            menuReglages();
        }
        if(menuChoisi == 2){
            itemSelect = 0;
            menuPrincipal();
        }
    }
    else if(idMenu == 2){ // Menu fin de partie
        if(menuChoisi==0){
            lancerJeu();
        }
        if(menuChoisi==1){
            itemSelect = 0;
            menuPrincipal();
        }
    }
}
void menuReglages(){
    while(true){
        system("cls");
        displayHeader();
        cout << "    MENU REGLAGES\n\n";
        cout << "Mode de placement des vehicules :          " << selectionMenu(0, itemSelect, true) << getModePlacement() << selectionMenu(0, itemSelect, false) << endl;
        cout << "Difficulte du niveau (si placement auto) : " << selectionMenu(1, itemSelect, true) << getDifficulte() << selectionMenu(1, itemSelect, false) << endl;
        cout << "                                           " << selectionMenu(2, itemSelect, true) << "Retour au menu principal" << selectionMenu(2, itemSelect, false) << endl;
        char userCmd = attendreActionClavier();
        if(userCmd == 'h') itemSelect--;
        else if(userCmd == 'b') itemSelect++;
        else if(userCmd == 'e'){
            choixMenu(1, itemSelect);
            break;
        } ;
        if(itemSelect<0) itemSelect = 2;
        else if(itemSelect>2) itemSelect = 0;
    }
}
string getModePlacement(){
    if(modeDePlacement == false) return "Manuel";
    else return "Automatique";
}
string getDifficulte(){
    if(niveauDeDiff == 0) return "Facile";
    if(niveauDeDiff == 1) return "Moyen";
    if(niveauDeDiff == 2) return "Difficile";
    else return NULL;
}
string selectionMenu(int item, int itemSelect, int debut){
    string result;
    if(item == itemSelect){
        if(debut) result = ">>";
        else result = "<<";
    }
    else result = "  ";
    return result;
}




// Fonctions créées à des fins de test et de debug //
void testClavier(){
    while(true){
        if(kbhit()){
            int ch = int(getch());
            cout << ch << endl;
        }
    }
}

/* Tests pour la résolution automatique

int movePaths[30000][10] = {0};
int idChemin = 0;

void test1(){
    for(int i=0; i<100; i++){
        for (int j = 0; j < 5; ++j){
            cout << movePaths[i][j] << ", ";
        }
        cout << endl;
    }
}
void suivreChemin(){
    int nbrMvts = nombreVehicules()*2;
    for(int i=0; i<nbrMvts; i++){
        generer_chemins(0, i, nbrMvts);
    }

    for(int i=0; i<100; i++){
        for(int niveau; niveau<5; niveau++){
            if(movePaths[i][niveau]%2==0){
                bouger(movePaths[i][niveau]/2, -1);
            }
            else bouger((movePaths[i][niveau]-1)/2, 1);
            
            rafraichir("test");
            Sleep(2);
         
        }
    }
    test1();
    while(true);
}
void generer_chemins(int niveau, int idMvt, int nbrMvts){
    movePaths[idChemin][niveau] = idMvt;
    cout << idChemin << ", " << niveau << ", " << idMvt << endl;
    if(niveau<5){
        for(int i=0; i<nbrMvts; i++){
            generer_chemins(niveau+1, i, nbrMvts);
        }
    }
    else idChemin++;
} */